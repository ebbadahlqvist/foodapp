//
//  ViewController.h
//  FoodApp
//
//  Created by Ebba on 2016-02-24.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *searchButton;

@property (weak, nonatomic) IBOutlet UIImageView *animatedImageView;

@end

