//
//  FoodsTableViewController.h
//  FoodApp
//
//  Created by Ebba on 2016-02-24.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FejkDB.h"

@interface FoodsTableViewController : UITableViewController
@property (nonatomic, strong)NSArray *foodArray;

@end
