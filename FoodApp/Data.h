//
//  Data.h
//  FoodApp
//
//  Created by Ebba on 2016-03-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Data : NSObject
@property () NSArray *foodArray;
@property () NSMutableArray *foodArrayWithProtein;
@property () NSNumber *foodId;
-(void)populateArray;
@end
