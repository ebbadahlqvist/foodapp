//
//  CustomFavouriteCell.h
//  FoodApp
//
//  Created by Ebba on 2016-03-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomFavouriteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *foodNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *caloriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;

@end
