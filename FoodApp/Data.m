//
//  Data.m
//  FoodApp
//
//  Created by Ebba on 2016-03-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "Data.h"

@implementation Data


-(void)populateArray{

    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff?nutrient"];
    NSURL *url = [NSURL URLWithString:s];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *  data, NSURLResponse *  response, NSError *  error) {
        NSLog(@"Request finished in DB!");
        
        if(error) {
            
            //somthing to warn the user that it doesn work etc Alert! The rest of the block wont go.
            NSLog(@"ERROR! %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSArray *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
        // loggar result om jag vill : NSLog(@"result : %@",result);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.foodArray = [[NSArray alloc]init];
            self.foodArray  = result;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataIsReady"
                                                                object:nil];
        });
        
    }];
    
    [task resume];
}

@end
