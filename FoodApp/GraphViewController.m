//
//  GraphViewController.m
//  FoodApp
//
//  Created by Ebba on 2016-03-15.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "GraphViewController.h"

@implementation GraphViewController

-(void)viewDidLoad{
    
    
    self.valuesArray = [[NSMutableArray alloc]init];
    self.namesArray = [[NSMutableArray alloc]init];
    
    self.foodOneLabel.text = self.foodOneName;
    self.foodTwoLabel.text = self.foodTwoName;
    
    NSLog(@"HÄMTAT1: %@",self.foodOne);
    NSLog(@"HÄMTAT2: %@",self.foodTwo);
    
    NSNumber *proteinOne = [self.foodOne objectForKey:@"protein"];
    NSNumber *sugerOne = [self.foodOne objectForKey:@"saccharose"];
    NSNumber *fatOne = [self.foodOne objectForKey:@"fat"];
    
    NSNumber *proteinTwo = [self.foodTwo objectForKey:@"protein"];
    NSNumber *sugerTwo = [self.foodTwo objectForKey:@"saccharose"];
    NSNumber *fatTwo = [self.foodTwo objectForKey:@"fat"];
    
    [self.valuesArray addObject:proteinOne];
    [self.valuesArray addObject:proteinTwo];
    [self.valuesArray addObject:sugerOne];
    [self.valuesArray addObject:sugerTwo];
    [self.valuesArray addObject:fatOne];
    [self.valuesArray addObject:fatTwo];
  
    NSString *proteinNameOne = @"   PROTEIN";
    NSString *sugerNameOne = @"  SOCKER";
    NSString *fatNameOne = @"   FETT";
    [self.namesArray addObject:proteinNameOne];
    [self.namesArray addObject:@""];
    [self.namesArray addObject:sugerNameOne];
    [self.namesArray addObject:@""];
    [self.namesArray addObject:fatNameOne];
    [self.namesArray addObject:@""];
    
    self.barGraph.dataSource = self;
    
    [self.barGraph draw];
    
    for (UILabel* lab in self.barGraph.labels){
    [lab sizeToFit];
        
    }
    
}

- (NSInteger)numberOfBars{
    return 6;
}
- (NSNumber *)valueForBarAtIndex:(NSInteger)index{
    NSNumber *value = [self.valuesArray objectAtIndex:index];
    double d = [value doubleValue]*3;
    NSNumber *newValue = [NSNumber numberWithDouble:d];
    return newValue;
}
- (NSString *)titleForBarAtIndex:(NSInteger)index{
    
    return [self.namesArray objectAtIndex:index];
}

- (UIColor *)colorForBarAtIndex:(NSInteger)index{
    UIColor *color;
    if (index % 2){
        
        color = [UIColor colorWithRed:0.373 green:0.812 blue:0.431 alpha:1];
        
    }else {
        
        color = [UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1];
    }
    
    return color;
}

@end
