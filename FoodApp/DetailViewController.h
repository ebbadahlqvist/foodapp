//
//  DetailViewController.h
//  FoodApp
//
//  Created by Ebba on 2016-02-24.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *sugerLabel;
@property (weak, nonatomic) IBOutlet UILabel *caloriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *cholesterolLabel;

@property (weak, nonatomic) IBOutlet UILabel *waterLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UISwitch *favouriteSwitch;

@property (strong, nonatomic) IBOutlet UILabel *pointsLabel;

@property () NSNumber *saccharoseAmount;
@property () NSNumber *caloriesAmount;
@property () NSNumber *cholestorolAmount;
@property () NSNumber *waterAmount;
@property () NSNumber *fatAmount;
@property () NSNumber *proteinAmount;
@property () NSNumber *foodId;
@property () NSString *foodName;

@property () NSDictionary *currentFood;
@property () NSDictionary *firstResult;
@property () NSDictionary *dictResult;

@property () NSMutableArray *favouritesArray;

@end
