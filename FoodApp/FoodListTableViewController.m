//
//  FoodListTableViewController.m
//  FoodApp
//
//  Created by Ebba on 2016-02-24.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "FoodListTableViewController.h"
#import "DetailViewController.h"
#import "CustomFoodCell.h"

@interface FoodListTableViewController ()
@property (strong, nonatomic) IBOutlet UINavigationItem *navBar;

@end

@implementation FoodListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *cancelButton = [self.searchController.searchBar valueForKey:@"_cancelButton"];
    [cancelButton setTitleColor:[UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1] forState:UIControlStateNormal];
    
    [self.searchController.searchBar setValue:@"Avbryt" forKey:@"_cancelButtonText"];
    CustomFoodCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    [cell.loading startAnimating];
    
    self.dB = [[Data alloc]init];
    [self.dB populateArray];
    self.arrayDb = [[NSArray alloc]init];
    self.searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.searchController.searchBar.barTintColor = [UIColor whiteColor];
    self.searchController.searchBar.placeholder = @"sök måltid här";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"DataIsReady"
                                               object:nil];
}

-(void)viewDidUnload{

[[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)reloadTable:(NSNotification *)notif {
    self.arrayDb = [NSArray arrayWithArray:[self.dB foodArray]];
    [self.tableView reloadData];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString *searchtext = searchController.searchBar.text;
    NSLog(@"searchText = %@", searchtext);
    
    NSPredicate *findFoodByName =
        [NSPredicate predicateWithFormat:@"name contains[c] %@", searchtext];
    
    self.searchResult = [self.arrayDb filteredArrayUsingPredicate:findFoodByName];
    
    
    [self.tableView reloadData];

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.searchController.isActive && self.searchController.searchBar.text.length > 0){
        return self.searchResult.count;
    } else {
    return self.arrayDb.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIButton *cancelButton = [self.searchController.searchBar valueForKey:@"_cancelButton"];
    [cancelButton setTitleColor:[UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1] forState:UIControlStateNormal];
    
    [self.searchController.searchBar setValue:@"Avbryt" forKey:@"_cancelButtonText"];
    
    CustomFoodCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.tag = indexPath.row;
    
    if (indexPath.row % 2){
        
        cell.backgroundColor = [UIColor whiteColor];
        
    }else {
        
        cell.backgroundColor = cell.backgroundColor = [UIColor colorWithRed:0.984 green:0.957 blue:0.957 alpha:1];
    }
    
    NSArray *activeArray;
    
    if(self.searchController.isActive && self.searchController.searchBar.text.length > 0){
        activeArray = self.searchResult;
    } else {
    
        activeArray = self.arrayDb;
        
    }
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name"  ascending:YES];
    NSArray *sortedActiveArray;
    sortedActiveArray=[activeArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    activeArray = [sortedActiveArray copy];
   
    self.dictDb= activeArray[indexPath.row];
    NSString *text = self.dictDb[@"name"];
    NSNumber *foodId = self.dictDb[@"number"];
    
    cell.loading.hidden = NO;
    [cell.loading startAnimating];
    
    cell.fatLabel.text = @"";
    cell.proteinLabel.text = @"";
    cell.caloriesLabel.text = @"";
    
    cell.nameLabel.text = text;
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1];
    [cell setSelectedBackgroundView:bgColorView];
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", foodId];
    
    NSURL *url = [NSURL URLWithString:s];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *  data, NSURLResponse *  response, NSError *  error) {
        NSLog(@"Request finished!");
        
        if(error) {
           
            NSLog(@"ERROR! %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSDictionary *firstResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
        // loggar result om jag vill : NSLog(@"result : %@",result);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(cell.tag == indexPath.row){
            NSDictionary *dictResult  = firstResult[@"nutrientValues"];
                
            cell.proteinLabel.text = [NSString stringWithFormat:@"%@ protein/ 100 mg",dictResult[@"protein"]];
            cell.fatLabel.text = [NSString stringWithFormat:@"%@ fat/ 100 mg",dictResult[@"fat"]];
            cell.caloriesLabel.text = [NSString stringWithFormat:@"%@ kalorier/ 100 mg",dictResult[@"energyKcal"]];
            cell.loading.hidden = YES;
            [cell.loading stopAnimating];
            }
            
        });
        
    }];
    
    [task resume];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"foodDetailSegue"]){
    NSArray *activeArray;
    
    if(self.searchController.isActive && self.searchController.searchBar.text.length > 0){
        activeArray = self.searchResult;
    } else {
        
        activeArray = self.arrayDb;
    }
        
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name"  ascending:YES];
    NSArray *sortedActiveArray;
    sortedActiveArray=[activeArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    activeArray = [sortedActiveArray copy];
    
    DetailViewController *transferViewController = segue.destinationViewController;
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
  
    self.dictDb= activeArray[indexPath.row];
    
    NSNumber *foodId = self.dictDb[@"number"];
    transferViewController.foodName =self.dictDb[@"name"];
    transferViewController.foodId = foodId;
    }
}

- (NSString*)imagePathWithId:(id)foodId {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = paths[0];
    return [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",foodId]];
}

    
@end
