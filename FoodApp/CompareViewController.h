//
//  CompareViewController.h
//  FoodApp
//
//  Created by Ebba on 2016-03-15.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "ViewController.h"
#import "Data.h"

@interface CompareViewController : ViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableViewFood;
@property (strong, nonatomic) NSArray * foodArray;
@property (strong, nonatomic) NSDictionary *mealToComapre;
@property (strong, nonatomic) NSDictionary *pickedMealToCompare;
@property (strong, nonatomic) Data *dB;
@property () NSArray *searchResult;
@property () NSDictionary *dictResult;
@property (strong, nonatomic)NSString *mealName;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *statButton;
@property () NSArray *sortedArray;
@property (strong, nonatomic) IBOutlet UILabel *pickedNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *sackarosLabel;
@property (strong, nonatomic) IBOutlet UILabel *caloriesLabel;
@property (strong, nonatomic) IBOutlet UILabel *cholestorolLabel;
@property (strong, nonatomic) IBOutlet UILabel *waterLabel;
@property (strong, nonatomic) IBOutlet UILabel *fatLabel;
@property (strong, nonatomic) IBOutlet UILabel *proteinLabel;
@property (strong, nonatomic) IBOutlet UILabel *pickedSackarosLabel;
@property (strong, nonatomic) IBOutlet UILabel *pickedCaloriesLabel;
@property (strong, nonatomic) IBOutlet UILabel *pickedCholesterolLabel;
@property (strong, nonatomic) IBOutlet UILabel *pickedWaterLabel;
@property (strong, nonatomic) IBOutlet UILabel *pickedFatLabel;
@property (strong, nonatomic) IBOutlet UILabel *pickedProteinLabel;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end
