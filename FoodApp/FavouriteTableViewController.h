//
//  FavouriteTableViewController.h
//  FoodApp
//
//  Created by Ebba on 2016-03-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomFavouriteCell.h"
#import "Data.h"
#import "DetailViewController.h"
@interface FavouriteTableViewController : UITableViewController

@property () NSMutableArray *favouritesArray;

@end
