//
//  DetailViewController.m
//  FoodApp
//
//  Created by Ebba on 2016-02-24.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "DetailViewController.h"
#import "CompareViewController.h"
#import "GraphViewController.h"
@implementation DetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"favouritesArray"];
   
    self.favouritesArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if(self.favouritesArray == nil){
        self.favouritesArray = [[NSMutableArray alloc]init];
    }
    
    for(int i = 0; i < self.favouritesArray.count; i++){
        NSLog(@"favoritID = %@ foodId = %@",[self.favouritesArray[i] objectForKey:@"id"], self.foodId);
        if([[self.favouritesArray[i] objectForKey:@"id"] doubleValue] == [self.foodId doubleValue]){
            [self.favouriteSwitch setOn:YES];
        }
    }
    
    self.sugerLabel.text = @"";
    self.caloriesLabel.text =  @"";
    self.cholesterolLabel.text =  @"";
    self.waterLabel.text = @"";
    self.fatLabel.text = @"";
    self.proteinLabel.text = @"";
    self.pointsLabel.text = @"";

    UIImage *empty = [UIImage imageNamed:@""];
    
    self.imageView.layer.cornerRadius = self.imageView.frame.size.height/2;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.layer.borderWidth = 0;
    
    self.imageView.image = empty;
    
    [self populateLabels];
    
   }

-(void)populateLabels{
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.foodId];
    
    NSURL *url = [NSURL URLWithString:s];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *  data, NSURLResponse *  response, NSError *  error) {
        NSLog(@"Request finished!");
        
        if(error) {
            
            //somthing to warn the user that it doesn work etc Alert! The rest of the block wont go.
            NSLog(@"ERROR! %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        self.firstResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
        // loggar result om jag vill : NSLog(@"result : %@",result);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            self.dictResult  = self.firstResult[@"nutrientValues"];
            NSLog(@"%@",self.firstResult);
            self.title = self.firstResult[@"name"];
            self.foodName =self.firstResult[@"name"];
            self.saccharoseAmount = self.dictResult[@"saccharose"];
            self.caloriesAmount = self.dictResult[@"carbohydrates"];
            self.cholestorolAmount = self.dictResult[@"cholesterol"];
            NSNumber *cholestorol =self.dictResult[@"cholesterol"];
            NSNumber *water =self.dictResult[@"water"];
            NSNumberFormatter *formatt = [[NSNumberFormatter alloc]init];
            [formatt setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatt setRoundingMode:NSNumberFormatterRoundUp];
            [formatt setMaximumFractionDigits:1];
            NSString *waterRounded = [NSString stringWithFormat:@"%@ vatten/100 mg",[formatt stringFromNumber:water]];
            
            NSString *cholestorolRounded = [NSString stringWithFormat:@"%@ kolesterol/100 mg",[formatt stringFromNumber:cholestorol]];
            self.waterLabel.text= waterRounded;
            self.fatAmount = self.dictResult[@"fat"];
            self.proteinAmount = self.dictResult[@"protein"];
            self.cholesterolLabel.text = cholestorolRounded;
            
            NSTimeInterval duration = 3;
            [UIView transitionWithView:self.imageView
                              duration:duration
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                UIImage *cachedImage = [UIImage imageWithContentsOfFile:[self imagePath]];
                                
                                if(cachedImage){
                                    [self.imageView.layer setBorderColor:[[UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1]CGColor]];
                                    self.imageView.layer.borderWidth = 4;
                                    self.imageView.image = cachedImage;
                                }else {
                                    self.imageView.image = [UIImage imageNamed:@"appleByEbba"];
                                }
                                
                            } completion:nil];
            
            self.sugerLabel.text = [NSString stringWithFormat:@"%@  sackaros/ 100 mg",self.saccharoseAmount];
            self.caloriesLabel.text = [NSString stringWithFormat:@"%@  kalorier/ 100 mg",self.caloriesAmount];
            self.fatLabel.text =[NSString stringWithFormat:@"%@  fett/ 100 mg",self.fatAmount];
            self.proteinLabel.text =[NSString stringWithFormat:@"%@  protein/ 100 mg",self.proteinAmount];
            self.pointsLabel.text = [NSString stringWithFormat:@"%d /av 10 nyttighetspoäng",[self getHealtPoints]];
            self.pointsLabel.textColor = [UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1];
        });
        
    }];
    
    [task resume];
    
    self.sugerLabel.text = [NSString stringWithFormat:@"%@ amount of suger",self.foodId];
    


}

- (IBAction)saveSwitch:(UISwitch *)sender {
    NSLog(@"Save Switch");
    
    if(self.favouriteSwitch.isOn){
        NSLog(@"On");
        
        NSDictionary *dict = @{@"name" : self.foodName, @"id" : self.foodId, @"calories" : self.caloriesAmount, @"fat": self.fatAmount, @"protein": self.proteinAmount};
        
         NSLog(@"before array to add with");
        NSMutableArray *arrayToAddWith = [[NSMutableArray alloc]initWithArray:self.favouritesArray];
        [arrayToAddWith addObject:dict];
        self.favouritesArray = [arrayToAddWith copy];
        NSLog(@"after array to add with");
        [self saveToFavouriteArray];
        NSLog(@"%@",self.favouritesArray.description);
    } else {
        NSLog(@"Off");
        
        NSMutableArray *arrayToDeleteWith = [[NSMutableArray alloc]initWithArray:self.favouritesArray];

        for (int i = 0; i < self.favouritesArray.count; i++){
        if([arrayToDeleteWith[i] objectForKey:@"id"] == self.foodId){
            [arrayToDeleteWith removeObjectAtIndex:i];
        }
        
        self.favouritesArray = [arrayToDeleteWith copy];
            [self saveToFavouriteArray];
    }
        
        NSLog(@"%@",self.favouritesArray.description);
}
}
-(void)saveToFavouriteArray{
    
    NSLog(@"Save to Favourite");
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:self.favouritesArray];
    [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"favouritesArray"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];

}
- (IBAction)takePicture:(UIButton *)sender {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Egen bild" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ta Bild" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
        
    }];
    
    UIAlertAction *extraAction = [UIAlertAction actionWithTitle:@"Från Album" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType =  UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:picker animated:YES completion:nil];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Tillbaka" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    alert.view.tintColor = [UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1];
    
    [alert addAction:defaultAction];
    [alert addAction:extraAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
   
    self.imageView.layer.cornerRadius = self.imageView.frame.size.height/2;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.layer.borderWidth = 0;
    [self.imageView.layer setBorderColor:[[UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1]CGColor]];
    self.imageView.layer.borderWidth = 4;
    self.imageView.image = image;

    NSData *imageData = UIImagePNGRepresentation(image);
    BOOL success = [imageData writeToFile:[self imagePath] atomically:YES];
    if(success) {
        NSLog(@"Saved image to user documents directory.");
    } else {
        NSLog(@"Failed to save image.");
    }
}

- (NSString*)imagePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = paths[0];
    return [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",self.foodId]];
}

- (void)ControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(int)getHealtPoints{
    int points = 0;
    

    if ([self.caloriesAmount intValue] < 80){
        points+=3;
    }
    
    if ([self.fatAmount intValue] < 5){
        points+=3;
    }
    
    if ([self.cholestorolAmount intValue] < 1){
        points++;
    }
    if ([self.saccharoseAmount intValue] < 5){
        points+=3;
    } else if([self.saccharoseAmount intValue] > 12){
        points = 0;
    }
    
    return points;

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"compareSegue"]){
    CompareViewController *transferViewController = segue.destinationViewController;
    
    transferViewController.mealName = self.firstResult[@"name"];
    transferViewController.mealToComapre = self.dictResult;

    }
}

@end
