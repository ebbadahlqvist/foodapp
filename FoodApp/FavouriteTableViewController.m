//
//  FavouriteTableViewController.m
//  FoodApp
//
//  Created by Ebba on 2016-03-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "FavouriteTableViewController.h"

@implementation FavouriteTableViewController

-(void)viewDidLoad{

    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"favouritesArray"];
    self.favouritesArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name"  ascending:YES];
    NSArray *sortedActiveArray;
    sortedActiveArray=[self.favouritesArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    self.favouritesArray = [sortedActiveArray copy];
    
    NSLog(@"Saved Array = %@", self.favouritesArray.description);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.favouritesArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"FavouriteCell";
    
    CustomFavouriteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *foodDict = self.favouritesArray[indexPath.row];
    NSString *foodName = [foodDict objectForKey:@"name"];
    NSString *calories = [foodDict objectForKey:@"calories"];
    NSString *protein = [foodDict objectForKey:@"protein"];
    NSString *fat = [foodDict objectForKey:@"fat"];
    
    if (indexPath.row % 2){
        
        cell.backgroundColor = [UIColor whiteColor];
        
    }else {
        
        cell.backgroundColor = [UIColor colorWithRed:0.984 green:0.957 blue:0.957 alpha:1];
    }
    
    cell.caloriesLabel.text =[NSString stringWithFormat:@"%@ kalorier/ 100 mg",calories];
    cell.proteinLabel.text =[NSString stringWithFormat:@"%@ protein/ 100 mg",protein];
    cell.fatLabel.text =[NSString stringWithFormat:@"%@ fat/ 100 mg",fat];
    cell.foodNameLabel.text = foodName;
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
    
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSMutableArray *arrayToDeleteWith = [[NSMutableArray alloc]initWithArray:self.favouritesArray];
        [arrayToDeleteWith removeObjectAtIndex:indexPath.row];
        self.favouritesArray = [arrayToDeleteWith copy];
        [self saveToFavouriteArray];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView reloadData];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }

}
/*For TableView Log the indexPath clicked.*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"FavouriteToDetailSegue"]){
                
        DetailViewController *transferViewController = segue.destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        NSNumber *foodId = [self.favouritesArray[indexPath.row] objectForKey:@"id"];
        
        transferViewController.foodId = foodId;
    }
}

-(void)saveToFavouriteArray{
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:self.favouritesArray];
    [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"favouritesArray"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


@end
