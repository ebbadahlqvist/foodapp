//
//  FoodListTableViewController.h
//  FoodApp
//
//  Created by Ebba on 2016-02-24.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data.h"


@interface FoodListTableViewController : UITableViewController <UISearchResultsUpdating>

@property (nonatomic) NSArray *foodList;
@property () NSArray *arrayDb;
@property () Data *dB;
@property () NSDictionary *dictDb;
@property () NSArray *searchResult;
@property () UISearchController *searchController;

@end
