//
//  ViewController.m
//  FoodApp
//
//  Created by Ebba on 2016-02-24.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *arrowImage = [UIImage imageNamed: @"arrowByEbba.png"];
    
    UIImageView *arrowView = [[UIImageView alloc]initWithFrame:CGRectMake(100, 100, 50, 100)];
    
    [arrowView setImage:arrowImage];
    
    arrowView.center = CGPointMake(self.view.center.x, 10);
    
    [self.view addSubview:arrowView];
    
    self.animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    self.gravity =[[UIGravityBehavior alloc]initWithItems:@[arrowView]];
    
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[arrowView]];
    [self.collision setTranslatesReferenceBoundsIntoBoundary:YES];
    
    CGPoint topEdge = CGPointMake(self.searchButton.frame.origin.x +
                                  self.searchButton.frame.size.width, self.searchButton.frame.origin.y);
    [_collision addBoundaryWithIdentifier:@"barrier"
                                fromPoint:self.searchButton.frame.origin
                                  toPoint:topEdge];
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    
    
}


-(void)animateApple{
    [UIView animateWithDuration:10.0
                          delay:2.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.animatedImageView.image = [UIImage imageNamed:@"appleByEbbaOriginalPink"];
                     }
                     completion:^(BOOL finished){
                         nil;
                     }];
}

-(void)animatePinkApple{
    [UIView animateWithDuration:2.0
                          delay:2.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.animatedImageView.image = [UIImage imageNamed:@"appleByEbba"];
                     }
                     completion:^(BOOL finished){
                         [self animateApple];
                     }];
}

@end
