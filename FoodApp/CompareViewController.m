//
//  CompareViewController.m
//  FoodApp
//
//  Created by Ebba on 2016-03-15.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "CompareViewController.h"
#import "Data.h"
#import "CustomCompareCell.h"
#import "GraphViewController.h"

@implementation CompareViewController

-(void)viewDidLoad{

    self.dB = [[Data alloc]init];
    [self.dB populateArray];
    
    self.foodArray = [[NSArray alloc]init];
    self.foodArray = [[self.dB foodArray] copy];
    
    self.nameLabel.text = self.mealName;
    
    [self.tableViewFood scrollRectToVisible:CGRectMake(0, 0, self.tableViewFood.frame.size.width, self.tableViewFood.frame.size.height) animated:NO];
    
    NSDictionary *dictResult  = [self.foodArray[1] objectForKey:@"name"];
    NSLog(@"%@",dictResult);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"DataIsReady"
                                               object:nil];
    [self.tableViewFood.layer setBorderColor:[[UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1]CGColor]];
    self.tableViewFood.layer.borderWidth = 4;

    
    self.tableViewFood.dataSource = self;
    self.tableViewFood.delegate = self;
    
    self.sackarosLabel.text = [NSString stringWithFormat:@"%@  sackaros/ 100 mg",[self.mealToComapre objectForKey:@"saccharose"]];
    self.caloriesLabel.text = [NSString stringWithFormat:@"%@  kalorier/ 100 mg",[self.mealToComapre objectForKey:@"carbohydrates"]];
    
    NSNumber *water =[self.mealToComapre objectForKey:@"water"];
    NSNumber *cholestorol = [self.mealToComapre objectForKey:@"cholesterol"];
    NSNumberFormatter *formatt = [[NSNumberFormatter alloc]init];
    [formatt setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatt setRoundingMode:NSNumberFormatterRoundUp];
    [formatt setMaximumFractionDigits:1];
    NSString *waterRounded = [NSString stringWithFormat:@"%@ vatten/100 mg",[formatt stringFromNumber:water]];
    NSString *cholestorolRounded = [NSString stringWithFormat:@"%@ kolesterol/100 mg",[formatt stringFromNumber:cholestorol]];
    self.cholestorolLabel.text =  cholestorolRounded;
    self.waterLabel.text = waterRounded;
    self.fatLabel.text = [NSString stringWithFormat:@"%@  fett/ 100 mg",[self.mealToComapre objectForKey:@"fat"]];
    self.proteinLabel.text = [NSString stringWithFormat:@"%@  protein/ 100 mg",[self.mealToComapre objectForKey:@"protein"]];
    
    self.pickedNameLabel.text = @"";
    self.pickedSackarosLabel.text = @"";
    self.pickedCaloriesLabel.text = @"";
    self.pickedCholesterolLabel.text = @"";
    self.pickedWaterLabel.text = @"";
    self.pickedFatLabel.text = @"";
    self.pickedProteinLabel.text = @"";
    
    
  
}

-(void)viewDidUnload{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)reloadTable:(NSNotification *)notif {
    self.foodArray = [NSArray arrayWithArray:[self.dB foodArray]];
    [self.tableViewFood reloadData];
    NSDictionary *dictResult  = [self.foodArray[1] objectForKey:@"name"];
    NSLog(@"%@",dictResult);
}

/*For TableView*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.foodArray.count;
}

/*For TableView Log the indexPath clicked.*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Testing %@",indexPath);
    self.pickedMealToCompare = self.foodArray[indexPath.row];
    self.statButton.enabled = YES;
    [self setCorrectFood];
    
    
}

/*Style for cells in tableRow*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    static NSString* cellId=@"Cell";
    CustomCompareCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if(cell==Nil){
        cell= [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
    }
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name"  ascending:YES];
    NSArray *sortedActiveArray;
    sortedActiveArray=[self.foodArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    self.foodArray = [sortedActiveArray copy];
    NSString *name = [self.foodArray[indexPath.row] objectForKey:@"name"];
    
    cell.nameLabel.text = name;
    
    
    if (indexPath.row % 2){
        
        cell.backgroundColor = [UIColor whiteColor];
        
    }else {
        
        cell.backgroundColor = cell.backgroundColor = [UIColor colorWithRed:0.984 green:0.957 blue:0.957 alpha:1];
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:1 green:0.58 blue:0.58 alpha:1];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}


-(void)setCorrectFood{
    
    NSNumber *foodId = [self.pickedMealToCompare objectForKey:@"number"];
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", foodId];
    
    NSURL *url = [NSURL URLWithString:s];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *  data, NSURLResponse *  response, NSError *  error) {
        NSLog(@"Request finished!");
        
        if(error) {
            
            //somthing to warn the user that it doesn work etc Alert! The rest of the block wont go.
            NSLog(@"ERROR! %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSDictionary *firstResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
        // loggar result om jag vill : NSLog(@"result : %@",result);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            self.dictResult  = firstResult[@"nutrientValues"];
            NSLog(@"%@",firstResult);
            NSString *name = firstResult[@"name"];
           
            NSNumber *sackaros = self.dictResult[@"saccharose"];
            NSNumber *carbohydrates = self.dictResult[@"carbohydrates"];
            NSNumber *cholesterol = self.dictResult[@"cholesterol"];
            NSNumber *water = self.dictResult[@"water"];
            NSNumber *fat = self.dictResult[@"fat"];
            NSNumber *protein = self.dictResult[@"protein"];
            
            NSNumberFormatter *formatt = [[NSNumberFormatter alloc]init];
            [formatt setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatt setRoundingMode:NSNumberFormatterRoundUp];
            [formatt setMaximumFractionDigits:1];
            NSString *waterRounded = [NSString stringWithFormat:@"%@ vatten/100 mg",[formatt stringFromNumber:water]];
            NSString *cholesterolRounded =[NSString stringWithFormat:@"%@ kolesterol/100 mg",[formatt stringFromNumber:cholesterol]];
            NSString *caloriesRounded =[NSString stringWithFormat:@"%@ kalorier/100 mg",[formatt stringFromNumber:carbohydrates]];
            
                       
            self.pickedNameLabel.text = name;
            self.pickedSackarosLabel.text = [NSString stringWithFormat:@"%@  sackaros/ 100 mg",sackaros];
            
            self.pickedCaloriesLabel.text = caloriesRounded;
            self.pickedCholesterolLabel.text = cholesterolRounded;
            self.pickedWaterLabel.text = waterRounded;
            self.pickedFatLabel.text =[NSString stringWithFormat:@"%@  fett/ 100 mg",fat];
            self.pickedProteinLabel.text =[NSString stringWithFormat:@"%@  protein/ 100 mg",protein];
            
        });
        
    }];
    
    [task resume];
    

    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"TEST");
    if([segue.identifier isEqualToString:@"graphSegue"]){
    GraphViewController *transferViewController = segue.destinationViewController;
        NSLog(@"Protein som skickas: %@",[self.mealToComapre objectForKey:@"protein"]);
        
        transferViewController.foodOne = self.mealToComapre;
        transferViewController.foodTwo = self.dictResult;
        transferViewController.foodOneName = self.mealName;
        transferViewController.foodTwoName = [self.pickedMealToCompare objectForKey:@"name"];
        NSLog(@"Skickas: %@ %@", self.mealName, [self.pickedMealToCompare objectForKey:@"name"]);
        
    }
    
}

@end
