//
//  CustomCompareCell.h
//  FoodApp
//
//  Created by Ebba on 2016-03-15.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCompareCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
