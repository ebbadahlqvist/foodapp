//
//  GraphViewController.h
//  FoodApp
//
//  Created by Ebba on 2016-03-15.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>

@interface GraphViewController : UIViewController <GKBarGraphDataSource>
@property (strong, nonatomic) IBOutlet GKBarGraph *barGraph;
@property (strong,nonatomic) NSMutableArray *valuesArray;
@property (strong,nonatomic)NSMutableArray *namesArray;
@property (strong, nonatomic) NSDictionary *foodOne;
@property (strong, nonatomic) NSDictionary *foodTwo;
@property (strong, nonatomic) IBOutlet UILabel *foodOneLabel;
@property (strong, nonatomic) IBOutlet UILabel *foodTwoLabel;
@property () NSString *foodOneName;
@property () NSString *foodTwoName;
- (NSInteger)numberOfBars;
- (NSNumber *)valueForBarAtIndex:(NSInteger)index;
- (NSString *)titleForBarAtIndex:(NSInteger)index;
@end
